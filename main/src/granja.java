
import java.util.Scanner;

public class granja {
    public static void main(String[] args) {


    }

    static float precioCerdos(float peso) {
        float precio = 0;
        if (peso < 50) {
            precio = peso * 12.56f;
        } else if (peso > 150 && peso < 200) {
            precio = peso * 13.50f;
        } else if (peso > 200) {
            precio = peso * 14.56f;
        }
        return precio;
    }


    static String obtenerNombre(Scanner scanner) {
        System.out.println("Introduzca el nombre del animal:");
        return scanner.nextLine();
    }

    static Float obtenerPeso(Scanner scanner) {
        System.out.println("Introduzca el peso del animal:");
        return scanner.nextFloat();
    }

    static String obtenerTipo(Scanner scanner) {
        System.out.println("Introduzca el tipo de animal");
        System.out.println("A._ Tipo vaca");
        System.out.println("B._Tipo oveja");
        System.out.println("C._Tipo cerdo");
        return scanner.nextLine();
    }


    static float precioVacas(float peso) {
        float precio = 0;

        if (peso <= 50) {
            precio = peso * 10.90f;
        } else if (peso > 50 && peso <= 150) {
            precio = peso * 12.20f;
        } else if (peso > 150 && peso <= 200) {
            precio = peso * 14.02f;
        } else if (peso > 200 && peso <= 300) {
            precio = peso * 15.54f;
        } else if (peso > 300) {
            precio = peso * 19.35f;
        }

        return precio;
    }

    static float calcularPesoOvejas(float peso) {
        float precio = 0;
        if (peso < 50) {
            precio = peso * 7.43f;
        } else if (peso > 50 && peso < 150) {
            precio = peso * 9.46f;
        }
        return precio;
    }

    static String obtenerLongitud(Scanner scanner) {
        System.out.println("Introduzca la longitud  del animal");
        return scanner.nextLine();
    }

    static int opcionMenu(Scanner scanner) {
        System.out.println("Seleccione una opción: ");
        System.out.println("\t1.-Insertar datos.");
        System.out.println("\t2.-Mostrar datos.");
        System.out.println("\t3.-Salir.");

        return scanner.nextInt();
    }


    static char categoriaSegunAnio(int anio) {

        char categoria=0;
        if ( anio == 2018 ){
            System.out.println("La categoria del animal es D");
            categoria = 'D';
        } else if (anio == 2019  ){
            System.out.println("La categoria del animal es C");
            categoria = 'C';
        } else if (anio == 2020 ) {
            System.out.println("La categoria del animal es B");
            categoria = 'B';
        } else if (anio == 2021 ){
            System.out.println("La categoria del animal es A");
            categoria = 'A';
        }
        return categoria;
    }

    static String[] obtenerDatos(Scanner scanner){
        String[] datos = {obtenerTipo(scanner),
                ObtenerIdentificador(scanner),
                Float.toString(calcularPrecioPorAnimal(scanner, obtenerPeso(scanner))),
                obtenerLongitud(scanner),
                String.valueOf(categoriaSegunAnio(ObtenerAnioDeNacimiento(scanner))),
                obtenerNombre(scanner)};

        return datos;
    }

    static void mostrarDatos(String[] datos){
        System.out.println("Los datos son del animal son:");
        String[] texto = {"Tipo: ", "Identificador: ", "Precio: ", "Longitud: ", "Categoria: ", "Nombre: "};
        for (int i=0 ; i < datos.length ; i++){
            System.out.print(texto[i]);
            System.out.println(datos[i]);
        }

    }


    static int ObtenerAnioDeNacimiento(Scanner scanner){

        System.out.println("Introduce el anio de nacimiento");
        int anio = scanner.nextInt();
        return anio;
    }

    static String ObtenerIdentificador(Scanner scanner){
        System.out.println("Introduce el identificador del animal");
        return scanner.nextLine();
    }

    static float calcularPrecioPorAnimal(Scanner scanner, float peso){
        float precio = 0;
        if (obtenerTipo(scanner).charAt(0) == 'A' || obtenerTipo(scanner).charAt(0) == 'V'){
            precio = precioVacas(peso);
        } else if (obtenerTipo(scanner).charAt(0) == 'B' || obtenerTipo(scanner).charAt(0) == 'O'){
            precio = calcularPesoOvejas(peso);
        } else if (obtenerTipo(scanner).charAt(0) == 'C' || obtenerTipo(scanner).charAt(0) == 'C'){
            precio = precioCerdos(peso);
        }

        return precio;
    }
}